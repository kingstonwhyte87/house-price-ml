# EDA - explanatory data analysis

import pandas as pd 
import numpy as np 
from sklearn import preprocessing
from sklearn.preprocessing import RobustScaler
from sklearn.ensemble import RandomForestRegressor
from sklearn.pipeline import make_pipeline
import xgboost as xgb
from sklearn import linear_model

dataset = pd.read_csv('train.csv', encoding='utf-8')
test_dataset = pd.read_csv('test.csv', encoding='utf-8')

# print(dataset.head())
dataset = dataset.drop('Id', 1)

columns = list(dataset.columns.values)
# print(columns)
test_columns = list(test_dataset.columns.values)
# print(test_columns)
# we cant feed all this data into our model, so we need to understand the data.
# the question is, what features actually contribute to the price of these houses
# I will be using two methods, one is by intuition and the other is by analysis
# by intuition, I feel the number of rooms, the fencing, the year sold, things like pool area contribute greatly to 
# the house price

train_labels = ['YrSold', 'LotArea', 'LotFrontage', 'MSSubClass','OverallCond','YearBuilt',
 '1stFlrSF','2ndFlrSF', 'HalfBath', 'FullBath','BsmtHalfBath','BsmtFullBath','GrLivArea','LowQualFinSF',
 'GarageArea','GarageCars','WoodDeckSF','OpenPorchSF','EnclosedPorch','3SsnPorch','ScreenPorch','PoolArea',
 'MiscVal','MoSold','MSZoning', 'Alley', 'Street', 'LotShape', 'Utilities', 'LotConfig', 'YearRemodAdd',
 'MasVnrArea', 'KitchenQual', 'TotRmsAbvGrd', 'Fence', 'MiscFeature','SaleType', 'SaleCondition',
  'PavedDrive','GarageCond','GarageQual', 'GarageFinish', 'GarageType','FireplaceQu', 'Fireplaces',
  'Functional','Electrical', 'CentralAir','HeatingQC','Heating','BsmtFinType2','BsmtFinType1','BsmtExposure',
  'BsmtCond','BsmtQual','Foundation','ExterCond','ExterQual','MasVnrType','Exterior2nd','Exterior1st','RoofMatl',
  'RoofStyle','HouseStyle','BldgType','Condition2','Condition1','Neighborhood','LandSlope','LandContour']

for tl in range(len(train_labels)):
    dataset[train_labels[tl]] = dataset[train_labels[tl]].fillna(0)
  #   print('filling LotFrontage')
  # print(dataset[train_labels[tl]].isnull().sum())
# print(dataset[['LotFrontage']].isnull().sum())


# now we need to do some feature engineering
# for ms zonning
# replace_map = {
#   'MSZoning': {
#     'RL': 1,
#     'RP': 2,
#     'A': 3,
#     'C': 4,
#     'FV': 5,
#     'I': 6,
#     'RH': 7,
#     'RM': 8
#   }
# }
# dzoning = dataset.replace(replace_map, inplace=True)

dataset['Street'] = np.where(dataset['Street'].str.contains('Grvl'),1, 0)



features_to_categorize = ['LotConfig', 'MSZoning', 'Alley', 'LotShape',
'Utilities', 'KitchenQual', 'Fence', 'MiscFeature','SaleType', 'SaleCondition',
 'PavedDrive', 'GarageCond', 'GarageQual','GarageFinish','GarageType', 'FireplaceQu', 'Functional','Electrical',
 'CentralAir','HeatingQC','Heating','BsmtFinType2','BsmtFinType1','BsmtExposure','BsmtCond','BsmtQual',
 'Foundation','ExterCond','ExterQual','MasVnrType','Exterior2nd','Exterior1st','RoofMatl','RoofStyle','HouseStyle'
 ,'BldgType','Condition2','Condition1','Neighborhood','LandSlope','LandContour']

for ft in features_to_categorize:
  dataset[ft] = dataset[ft].astype('category')
  dataset[ft] = dataset[ft].cat.codes

# print(dataset.corr())
# print(dataset[['MasVnrArea']])
# print(dataset[['Street']].isnull().sum())
x_data = dataset[train_labels]
y_data = dataset[['SalePrice']]

x_data = x_data.to_numpy()
y_data = y_data.to_numpy().reshape(-1)


class StackedModel():
  def __init__(self):
    self.lasso = make_pipeline(RobustScaler(), linear_model.Lasso(alpha=0.1))
    self.model_xgb = xgb.XGBRegressor(colsample_bytree=0.4603, gamma=0.0468, 
                            learning_rate=0.05, max_depth=3, 
                            min_child_weight=1.7817, n_estimators=2200,
                            reg_alpha=0.4640, reg_lambda=0.8571,
                            subsample=0.5213, silent=1,
                            random_state =7, nthread = -1)
    self.randomf = make_pipeline(
      RobustScaler(),
      RandomForestRegressor(max_depth=20, random_state=7,n_estimators=100)
    )

  def Predict(self, X):
    # this function runs the model in a forward derection, from the base model to the metal model
    lasso_predictions = []
    for data in X:
      p = self.lasso.predict(data.reshape(1,-1))
      lasso_predictions.append(p)

    xgb_predictions = []
    for data_from_lasso in lasso_predictions:
      xgb_pred = self.model_xgb.predict([data_from_lasso])
      xgb_predictions.append(xgb_pred[0])

    random_forest_predictions = []
    for data_from_xgb in xgb_predictions:
      rand_pred = self.randomf.predict(data_from_xgb.reshape(-1,1))
      random_forest_predictions.append(rand_pred[0])
    return random_forest_predictions

  def Train(self, X, Y):
    #fit the lasso model
    self.lasso.fit(X, Y)
    score_lasso = self.lasso.score(X,Y)
    lasso_predictions = []
    for data in X:
      p = self.lasso.predict(data.reshape(1,-1))
      lasso_predictions.append(p)
    
    # fit the xgboost model
    self.model_xgb.fit(lasso_predictions, Y)
    score_xgb = self.model_xgb.score(lasso_predictions, Y)
    xgb_predictions = []
    for data_from_lasso in lasso_predictions:
      xgb_pred = self.model_xgb.predict([data_from_lasso])
      xgb_predictions.append(xgb_pred)

    # fit the random forest model
    self.randomf.fit(xgb_predictions, Y)
    score_randf = self.randomf.score(xgb_predictions,Y)
    random_forest_predictions = []
    for data_from_xgb in xgb_predictions:
      rand_pred = self.randomf.predict([data_from_xgb])
      random_forest_predictions.append(rand_pred)

    return score_lasso,score_xgb,score_randf

stackedModel = StackedModel()
scores = stackedModel.Train(x_data, y_data)
print(scores)


for tl in range(len(train_labels)):
    test_dataset[train_labels[tl]] = test_dataset[train_labels[tl]].fillna(0)
test_dataset['Street'] = np.where(test_dataset['Street'].str.contains('Grvl'),1, 0)

for ft in features_to_categorize:
  test_dataset[ft] = test_dataset[ft].astype('category')
  test_dataset[ft] = test_dataset[ft].cat.codes
test_x_data = test_dataset[train_labels]


Ids = test_dataset[['Id']]

tests = test_x_data.to_numpy()
predictions = []
for test in tests:
  p = stackedModel.Predict(test.reshape(1,-1))
  predictions.append(p[0])
# print(predictions)


# result = pd.DataFrame(Ids, columns=['Id'])
# result['SalePrice'] = predictions
# result.to_csv(path_or_buf='./result4_stacked.csv', encoding='utf-8', index=False)
# print(result)