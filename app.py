import pandas as pd 
import numpy as np 
from sklearn import preprocessing, linear_model

clf = linear_model.Lasso(alpha=0.1)

test_data = pd.read_csv('test.csv', encoding='utf-8')
dataset = pd.read_csv('train.csv', encoding='utf-8')
test_y = pd.read_csv('sample_submission.csv', encoding='utf-8')


# print(dataset.head())
dataset = dataset.drop('Id', 1)
test_data = test_data.drop('Id', 1)

columns = list(dataset.columns.values)
# print(columns)
print(test_y.columns.values)
# we cant feed all this data into our model, so we need to understand the data.
# the question is, what features actually contribute to the price of these houses
# I will be using two methods, one is by intuition and the other is by analysis
# by intuition, I feel the number of rooms, the fencing, the year sold, things like pool area contribute greatly to 
# the house price

train_labels = ['YrSold', 'LotArea', 'LotFrontage', 'MSSubClass']


for tl in range(len(train_labels)):
  if train_labels[tl] == 'LotFrontage':
    dataset[train_labels[tl]] = dataset[train_labels[tl]].fillna(0)
    print('filling LotFrontage')
  print(dataset[train_labels[tl]].isnull().sum())
print(dataset[['LotFrontage']].isnull().sum())



for tl in range(len(train_labels)):
  if train_labels[tl] == 'LotFrontage':
    test_data[train_labels[tl]] = test_data[train_labels[tl]].fillna(0)
    print('filling LotFrontage')
  print(test_data[train_labels[tl]].isnull().sum())
print(test_data[['LotFrontage']].isnull().sum())

x_data = dataset[['YrSold', 'LotArea', 'LotFrontage', 'MSSubClass']]
xtest_data = test_data[['YrSold', 'LotArea', 'LotFrontage', 'MSSubClass']]
ytest_data = test_y[['SalePrice']]
y_data = dataset[['SalePrice']]

inputs = x_data.to_numpy()
targets = y_data.to_numpy()
test_inputes = xtest_data.to_numpy()
test_targets = ytest_data.to_numpy()

# reshape data to be sent to model
inputs = inputs.reshape(inputs.shape[0], 4)
test_inputes = test_inputes.reshape(test_inputes.shape[0], 4)

x_test = inputs[1].reshape(1, -1)

y_test = targets[1]

# print(targets.shape)
# print(inputs.shape)
clf.fit(inputs, targets)
# print(clf.get_params())

pred = clf.predict(x_test)

# scaore = clf.score(test_inputes,test_targets)
print(test_inputes.shape)
score = clf.score(test_inputes, test_targets)
print(score)