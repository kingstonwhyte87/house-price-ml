# EDA - explanatory data analysis

import pandas as pd 
import numpy as np 
from sklearn import preprocessing
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers


dataset = pd.read_csv('train.csv', encoding='utf-8')
test_dataset = pd.read_csv('test.csv', encoding='utf-8')

# print(dataset.head())
dataset = dataset.drop('Id', 1)

columns = list(dataset.columns.values)
# print(columns)
test_columns = list(test_dataset.columns.values)
# print(test_columns)
# we cant feed all this data into our model, so we need to understand the data.
# the question is, what features actually contribute to the price of these houses
# I will be using two methods, one is by intuition and the other is by analysis
# by intuition, I feel the number of rooms, the fencing, the year sold, things like pool area contribute greatly to 
# the house price

train_labels = ['YrSold', 'LotArea', 'LotFrontage', 'MSSubClass','OverallCond','YearBuilt',
 '1stFlrSF','2ndFlrSF', 'HalfBath', 'FullBath','BsmtHalfBath','BsmtFullBath','GrLivArea','LowQualFinSF',
 'GarageArea','GarageCars','WoodDeckSF','OpenPorchSF','EnclosedPorch','3SsnPorch','ScreenPorch','PoolArea',
 'MiscVal','MoSold','MSZoning', 'Alley', 'Street', 'LotShape', 'Utilities', 'LotConfig', 'YearRemodAdd',
 'MasVnrArea', 'KitchenQual', 'TotRmsAbvGrd', 'Fence', 'MiscFeature','SaleType', 'SaleCondition',
  'PavedDrive','GarageCond','GarageQual', 'GarageFinish', 'GarageType','FireplaceQu', 'Fireplaces',
  'Functional','Electrical', 'CentralAir','HeatingQC','Heating','BsmtFinType2','BsmtFinType1','BsmtExposure',
  'BsmtCond','BsmtQual','Foundation','ExterCond','ExterQual','MasVnrType','Exterior2nd','Exterior1st','RoofMatl',
  'RoofStyle','HouseStyle','BldgType','Condition2','Condition1','Neighborhood','LandSlope','LandContour']

for tl in range(len(train_labels)):
    dataset[train_labels[tl]] = dataset[train_labels[tl]].fillna(0)


dataset['Street'] = np.where(dataset['Street'].str.contains('Grvl'),1, 0)



features_to_categorize = ['LotConfig', 'MSZoning', 'Alley', 'LotShape',
'Utilities', 'KitchenQual', 'Fence', 'MiscFeature','SaleType', 'SaleCondition',
 'PavedDrive', 'GarageCond', 'GarageQual','GarageFinish','GarageType', 'FireplaceQu', 'Functional','Electrical',
 'CentralAir','HeatingQC','Heating','BsmtFinType2','BsmtFinType1','BsmtExposure','BsmtCond','BsmtQual',
 'Foundation','ExterCond','ExterQual','MasVnrType','Exterior2nd','Exterior1st','RoofMatl','RoofStyle','HouseStyle'
 ,'BldgType','Condition2','Condition1','Neighborhood','LandSlope','LandContour']

for ft in features_to_categorize:
  dataset[ft] = dataset[ft].astype('category')
  dataset[ft] = dataset[ft].cat.codes

# print(dataset.corr())
# print(dataset[['MasVnrArea']])
# print(dataset[['Street']].isnull().sum())
x_data = dataset[train_labels]
y_data = dataset[['SalePrice']]

# from sklearn import linear_model
# clf = linear_model.Lasso(alpha=0.1)
# clf.fit(x_data.to_numpy(), y_data.to_numpy())
# score = clf.score(x_data.to_numpy(), y_data.to_numpy())
# h = clf.predict(x_data.to_numpy()[0].reshape(1,-1))
# print(score)

train_stats = dataset.describe()
train_stats = train_stats.transpose()

def norm(x):
  return (x - train_stats['mean']) / train_stats['std']
normed_train_data = norm(x_data)
# print(normed_train_data.head())

len(x_data.keys())
def build_model():
  model = keras.Sequential([
    layers.Dense(64, activation='relu'),
    layers.Dense(64, activation='relu'),
    layers.Dense(1)
  ])

  optimizer = tf.keras.optimizers.RMSprop(0.001)

  model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae', 'mse'])
  return model
model = build_model()
# model.summary()

history = model.fit(
  normed_train_data.to_numpy(), y_data.to_numpy(),
  )
print(history)
# print(normed_train_data.head())
# for tl in range(len(train_labels)):
#     test_dataset[train_labels[tl]] = test_dataset[train_labels[tl]].fillna(0)
# test_dataset['Street'] = np.where(test_dataset['Street'].str.contains('Grvl'),1, 0)

# for ft in features_to_categorize:
#   test_dataset[ft] = test_dataset[ft].astype('category')
#   test_dataset[ft] = test_dataset[ft].cat.codes
# test_x_data = test_dataset[train_labels]


# Ids = test_dataset[['Id']]

# tests = test_x_data.to_numpy()
# predictions = []
# for test in tests:
#   p = clf.predict(test.reshape(1,-1))
#   predictions.append(p[0])

# submissions = {
#   'SalePrice': predictions,
#   'Id': Ids
# }
# result = pd.DataFrame(Ids, columns=['Id'])
# result['SalePrice'] = predictions
# # result.to_csv(path_or_buf='./result2.csv', encoding='utf-8', index=False)
# print(result)