# EDA - explanatory data analysis

import pandas as pd 
import numpy as np 
from sklearn import preprocessing
import torch.nn.functional as F
import torch
import torch.nn as nn
from torchvision import datasets, transforms
from torch.utils.data import TensorDataset, DataLoader


dataset = pd.read_csv('train.csv', encoding='utf-8')
test_dataset = pd.read_csv('test.csv', encoding='utf-8')

# print(dataset.head())
dataset = dataset.drop('Id', 1)

columns = list(dataset.columns.values)
# print(columns)
test_columns = list(test_dataset.columns.values)
# print(test_columns)
# we cant feed all this data into our model, so we need to understand the data.
# the question is, what features actually contribute to the price of these houses
# I will be using two methods, one is by intuition and the other is by analysis
# by intuition, I feel the number of rooms, the fencing, the year sold, things like pool area contribute greatly to 
# the house price

train_labels = ['YrSold', 'LotArea', 'LotFrontage', 'MSSubClass','OverallCond','YearBuilt',
 '1stFlrSF','2ndFlrSF', 'HalfBath', 'FullBath','BsmtHalfBath','BsmtFullBath','GrLivArea','LowQualFinSF',
 'GarageArea','GarageCars','WoodDeckSF','OpenPorchSF','EnclosedPorch','3SsnPorch','ScreenPorch','PoolArea',
 'MiscVal','MoSold','MSZoning', 'Alley', 'Street', 'LotShape', 'Utilities', 'LotConfig', 'YearRemodAdd',
 'MasVnrArea', 'KitchenQual', 'TotRmsAbvGrd', 'Fence', 'MiscFeature','SaleType', 'SaleCondition',
  'PavedDrive','GarageCond','GarageQual', 'GarageFinish', 'GarageType','FireplaceQu', 'Fireplaces',
  'Functional','Electrical', 'CentralAir','HeatingQC','Heating','BsmtFinType2','BsmtFinType1','BsmtExposure',
  'BsmtCond','BsmtQual','Foundation','ExterCond','ExterQual','MasVnrType','Exterior2nd','Exterior1st','RoofMatl',
  'RoofStyle','HouseStyle','BldgType','Condition2','Condition1','Neighborhood','LandSlope','LandContour']

for tl in range(len(train_labels)):
    dataset[train_labels[tl]] = dataset[train_labels[tl]].fillna(0)
  #   print('filling LotFrontage')
  # print(dataset[train_labels[tl]].isnull().sum())
# print(dataset[['LotFrontage']].isnull().sum())


# now we need to do some feature engineering
# for ms zonning
# replace_map = {
#   'MSZoning': {
#     'RL': 1,
#     'RP': 2,
#     'A': 3,
#     'C': 4,
#     'FV': 5,
#     'I': 6,
#     'RH': 7,
#     'RM': 8
#   }
# }
# dzoning = dataset.replace(replace_map, inplace=True)

dataset['Street'] = np.where(dataset['Street'].str.contains('Grvl'),1, 0)



features_to_categorize = ['LotConfig', 'MSZoning', 'Alley', 'LotShape',
'Utilities', 'KitchenQual', 'Fence', 'MiscFeature','SaleType', 'SaleCondition',
 'PavedDrive', 'GarageCond', 'GarageQual','GarageFinish','GarageType', 'FireplaceQu', 'Functional','Electrical',
 'CentralAir','HeatingQC','Heating','BsmtFinType2','BsmtFinType1','BsmtExposure','BsmtCond','BsmtQual',
 'Foundation','ExterCond','ExterQual','MasVnrType','Exterior2nd','Exterior1st','RoofMatl','RoofStyle','HouseStyle'
 ,'BldgType','Condition2','Condition1','Neighborhood','LandSlope','LandContour']

for ft in features_to_categorize:
  dataset[ft] = dataset[ft].astype('category')
  dataset[ft] = dataset[ft].cat.codes

# print(dataset.corr())
# print(dataset[['MasVnrArea']])
# print(dataset[['Street']].isnull().sum())
x_data = dataset[train_labels]
y_data = dataset[['SalePrice']]



scaler = preprocessing.Normalizer(norm='l1')
X = scaler.fit_transform(x_data)
Y = scaler.fit_transform(y_data)
X = x_data.to_numpy()

Y = y_data.to_numpy()

print(X.shape)
# x_train = torch.Tensor(X)
# y_train = torch.Tensor(Y)

transform = transforms.Compose(
  transforms.Normalize((0.5,),(0.5,))
)
train_data = TensorDataset(torch.from_numpy(X).float(), torch.from_numpy(Y).float())
batch_size = 30
train_loader = DataLoader(train_data, batch_size, True)
dataiter = iter(train_loader)
sample_x, sample_y = dataiter.next()
# print(sample_x)
# print(sample_y)
class Network(nn.Module):
  def __init__(self):
    super(Network, self).__init__()
    self.fc1 = nn.Linear(70, 50)
    self.fc2 = nn.Linear(50,30)
    self.fc3 = nn.Linear(30,20)
    self.fc4 = nn.Linear(20,10)
    self.fc5 = nn.Linear(10,1)

  def forward(self, x):
    # x = x.view(x.shape[0], -1)
    x = F.relu(self.fc1(x))
    x = F.relu(self.fc2(x))
    x = F.relu(self.fc3(x))
    x = F.relu(self.fc4(x))
    x = F.relu(self.fc5(x))
    return x


# print(X.shape)
# model = torch.nn.Sequential(
#   torch.nn.Linear(4, 2),
#   torch.nn.Linear(2, 1),
# )
model = Network()
criterion = torch.nn.MSELoss()

optimizer = torch.optim.Adam(model.parameters(), lr=0.05)
# print(model)

epoch = 1000
for e in range(epoch):
  running_loss = 0
  for data, output in train_loader:
    optimizer.zero_grad()
    h = model(data)
    loss = criterion(h, output)
    loss.backward()
    optimizer.step()
    running_loss += loss.item()
    # print(h)
  # print(running_loss)




for tl in range(len(train_labels)):
    test_dataset[train_labels[tl]] = test_dataset[train_labels[tl]].fillna(0)
test_dataset['Street'] = np.where(test_dataset['Street'].str.contains('Grvl'),1, 0)

for ft in features_to_categorize:
  test_dataset[ft] = test_dataset[ft].astype('category')
  test_dataset[ft] = test_dataset[ft].cat.codes
test_x_data = test_dataset[train_labels]


Ids = test_dataset[['Id']]

tests = test_x_data.to_numpy()
predictions = []
for test in tests:
  p = model(torch.from_numpy(test.reshape(1,-1)).float())
  predictions.append(p.detach().numpy()[0][0])

submissions = {
  'SalePrice': predictions,
  'Id': Ids
}
result = pd.DataFrame(Ids, columns=['Id'])
result['SalePrice'] = predictions
result.to_csv(path_or_buf='./result5_neuralnet2.csv', encoding='utf-8', index=False)
print(result)









test_one = sample_x[0]
real = sample_y[0]
# print(real)
h = model(test_one)
# print(h)
